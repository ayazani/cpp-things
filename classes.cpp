#include <iostream>
#include "./MSTRING"
#include "./ARRAY.hh"
#include "cstring"
//#include "string"

//Проверка присутствия символа sym в строке razd
int IsRazd(char sym, const char *razd)
{
    for(int i=0;razd[i];i++)
        if(razd[i]==sym)
            return 1;
    return 0;
}

//Аналог стандартной функции strtok, выделяет и возвращает фрагменты str,
// ограниченные символами из razd, но при этом не портит строку str.

char* StrTok(const char *str, const char *razd)
{
    static char *p=nullptr, *p1, *p2;
    char *p3;
    int state=1;
    int len;

    if(str)
    {
        for(len=0;str[len];len++);
        p=new char[len+1];
        for(int k=0; (p[k]=str[k]); k++);
        p1=p2=p;
    }

    while(true)
    {
        if(p==nullptr) return nullptr;
        switch(state)
        {
            case 1:
                if(*p2=='\0')
                { delete[] p; p=nullptr; return nullptr;}
                if(IsRazd(*p2,razd))
                {p1=++p2; break;}
                state=2;
            case 2:
                if(*p2=='\0' || IsRazd(*p2,razd))
                {
                    p3=p1;
                    if(*p2=='\0') return p3;
                    *p2='\0'; p1=++p2; return p3;
                }
                p2++;
        }
    }
}


class Employee_Vacation {
    //поля
    MString fio, department, vacancy; //Фио, название подразделения, должность
    Array<MString> vacation_list; // список отпусков
    public:
    //конструкторы
    Employee_Vacation (const char* fio, const char* department, const char* vacancy, const char* vacation_list );
    //корректировка данных
    MString* CorrectData (const char* name);
    //запись в лист отпусков
    void AddVacation (const char* newRecord);
    //корректировка листа отпусков
    void CorrectVacationList (int numberRecord, const char* newRecord);
    // перегрузка =
    Employee_Vacation & operator = (const Employee_Vacation &);
    //перегрузка <<
    friend std::ostream & operator << (std::ostream &, Employee_Vacation &);

};

Employee_Vacation::Employee_Vacation(const char* FIO, const char* Department, const char* Vacancy,
                                     const char* Vacation_list) : fio(FIO), department(Department), vacancy(Vacancy) {
    char *p;
    p = StrTok(Vacation_list, ";");
    if (p) {
        vacation_list.Add(p);
        while ((p=StrTok(nullptr, ";")))
            vacation_list.Add(p);
    }
}

MString *Employee_Vacation::CorrectData(const char *name) {
    if (!strcmp(name, "FIO"))
        return &fio;
    if(!strcmp(name, "Department"))
        return &department;
    if(!strcmp(name, "Vacancy"))
        return &vacancy;
    return nullptr;
}

void Employee_Vacation::AddVacation(const char *newRecord) {
    char *p;
    p = StrTok(newRecord, ";");
    if(p) {
        vacation_list.Add(p);
        while ((p = StrTok(nullptr, ";")))
            vacation_list.Add(p);
    }
}

void Employee_Vacation::CorrectVacationList(int numberRecord, const char *newRecord) {
    if (newRecord == nullptr){
        vacation_list.Erase(numberRecord);
    }
    else {
        vacation_list.Insert(newRecord, numberRecord);
    }
}

Employee_Vacation&Employee_Vacation::operator=(const Employee_Vacation &employeeVacation) {
    fio = employeeVacation.fio;
    vacancy = employeeVacation.vacancy;
    department = employeeVacation.department;
    vacation_list = employeeVacation.vacation_list;
    return *this;
}

std::ostream&operator<<(std::ostream &ostream, Employee_Vacation &employeeVacation) {
    int size, i = 0;
    ostream << "FIO: " << employeeVacation.fio << std::endl;
    ostream << "Department: " << employeeVacation.department << std::endl;
    ostream << "Vacancy: " << employeeVacation.vacancy << std::endl;
    size = employeeVacation.vacation_list.Size();
    if (size) {
        ostream << "Vacation list: " << std::endl;
        for (; i < size; i++) {
            ostream << i + 1 << ") " << employeeVacation.vacation_list[i];
            ostream << std::endl;
        }
    }
    return ostream;
}


int main() {
    using namespace std;
    setlocale(LC_ALL, "");
    //создание объектов на основе классов и использование методов для корректировки, перегрузок вывода и присваивания
    Employee_Vacation Dave( "David Blain", "IT", "backend programmer", "02.03.19 - 16.03.19; 09.09.19 - 23.09.19");
    cout << Dave << endl;
    Employee_Vacation Mario ("Mario", "Game development", "Victim", "02.02.18 - 16.02.18; 04.10.19 - 18.10.19; 20.01.20 - 01.02.20");
    cout << Mario << endl;
    Employee_Vacation Luigi(Mario);
    cout << Luigi << endl;

    *Luigi.CorrectData("FIO") = MString("Luigi");
    Luigi.CorrectVacationList(0, nullptr);
    Luigi.CorrectVacationList(0, "Correcting vacation");
    Luigi.AddVacation("new vacation??");
    cout << Luigi << endl;

    return 0;
}