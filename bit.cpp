#include <iostream>

using namespace std;

unsigned ChangeBits (unsigned x, unsigned y) {
    const unsigned mask = 0xaaaaaaaa;
    return x | (y & mask);
}
int main() {
    setlocale(LC_ALL, "");
    unsigned a, b;
    cout << "Введите значение 1" << endl;
    cin >> a;
    cout << "Введите значение 2" << endl;
    cin >> b;
    cout << ChangeBits(a, b);
    return 0;
}