#include <iostream>
#include <vector>
using namespace std;
/*
 * Необходимо выполнить следующее задание:

1.	Разработать функтор,
 позволяющий собирать статистику о последовательности целых чисел
 (послед может содержать числа от -500 до 500).
 Функтор после обработки последовательности алгоритмом for_each должен предоставлять следующую статистику:
a.	Максимальное число в последовательности
b.	Минимальное число в последовательности
c.	Среднее число в последовательности
d.	Количество положительных чисел
e.	Количество отрицательных чисел
f.	Сумму нечетных элементов последовательности
g.	Сумму четных элементов последовательности
h.	Совпадают ли первый и последний элементы последовательности.

Проверить работу программы на случайно сгенерированных последовательностях.


 */

class Functor{

public:
    int max = 0, min = 0, odd = 0, even = 0, positive = 0, negative = 0;
    double average, sum = 0;
    int count = 0;
    bool firstTOlast;
    void operator() (const vector<int>& asset){
        for(auto i : asset){
            if(min > i) min = i;
            if(max < i) max = i;

            count++;
            sum += i;

            if(i%2 == 0)even += i;
            else odd += i;

            if(i<0)negative++;
            else if(i>0) positive++;
        }
        average = sum/count;
        firstTOlast = asset.begin()==asset.end();
        cout << endl << "Статистика о последовательности: " << endl;
        cout << "Максимальное число в последовательности: " << max << endl;
        cout << "Минимальное число в последовательности: " << min << endl;
        cout << "Среднее арифметическое последовательности: " << average << endl;
        cout << "Сумма четных чисел в последовательности: " << even << endl;
        cout << "Сумма нечетных чисел в последовательности: " << odd << endl;
        cout << "Положительных чисел в последовательности: " << positive << endl;
        cout << "Отрицательных чисел в последовательности: " << negative << endl;
        cout << "Равны ли начало и конец последовательности: ";
        if(firstTOlast){cout <<"Да";}else{cout <<"Нет";}

    }
};

int main() {
    int n;
    Functor functor;
    vector <int> asset;
    cout << "Введите длину последовательности: ";
    cin >> n;
    for (int i = 0; i < n; i++){
        asset.push_back(500-rand()%1001);
    }
    functor(asset);
    return 0;
}
