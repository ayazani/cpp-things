#include <iostream>
#include <vector>

using namespace std;
/*
 *
2.	Написать программу, которая выполняет следующие действия
 (все операции должны выполняться с помощью стандартных алгоритмов):
a.	Заполняет вектор геометрическими фигурами.
 Геометрическая фигура может быть треугольником, квадратом, прямоугольником или пяти угольником.
 Структура описывающая геометрическую фигуру  определена ниже,
b.	Подсчитывает общее количество вершин всех фигур содержащихся в векторе
 (так треугольник добавляет к общему числу 3, квадрат 4 и т.д.)
c.	Подсчитывает количество треугольников, квадратов и прямоугольников в векторе
d.	Удаляет все пятиугольники
e.	На основании этого вектора создает vector<Point>,
 который содержит координаты одной из вершин (любой) каждой фигуры,
 т.е. первый элемент этого вектора содержит координаты одной из вершину первой фигуры,
 второй элемент этого вектора содержит координаты одной из вершину второй фигуры и т.д.
f.	Изменяет вектор так, чтобы он содержал в начале все треугольники, потом все квадраты, а потом прямоугольники.
g.	Распечатывает вектор после каждого этапа работы


Геометрическая фигура задается следующей структурой:
typedef  struct
{
    int vertex_num;      // количество вершин, для треугольника 3, для квадрата и
                                   // прямоугольника 4, для пяти угольника 5
    vector<Point> vertexes;   // вектор содержащий координаты вершин фигуры
    // Для треугольника содержит 3 элемента
 				    // Для квадрата и прямоугольника содержит 4
                                             // элемента
                                             // Для пятиугольника 5 элементов
} Shape;

typedef struct
{
    int x,y;
} Point;


Подсказка: кроме алгоритмов рассмотренных в этой работе можно применять все средства описанные в предыдущих работах,
 включая алгоритмы сортировки.

 */
typedef struct
{
    int x,y;
} Point;

typedef  struct
{
    int vertex_num;
    vector<Point> vertexes;
} Shape;

class Task{
public:
    vector <Shape> Shapes;
    Task() = default;
    ~Task() = default;
    void FillVector(){
        Shape TmpShape;
        int angles;
        Point coordinates;
        angles = rand()%3 + 3;
        switch(angles){
            case 3:
                for(int i = 0; i < 3; i++){
                    coordinates.x = rand()%20;
                    coordinates.y = rand()%20;
                    TmpShape.vertexes.push_back(coordinates);
                }
                TmpShape.vertex_num = 3;
                break;
            case 4:
                for(int i = 0; i < 4; i++){
                    coordinates.x = rand()%20;
                    coordinates.y = rand()%20;
                    TmpShape.vertexes.push_back(coordinates);
                }
                TmpShape.vertex_num = 4;
                break;
            case 5:
                for(int i = 0; i < 5; i++){
                    coordinates.x = rand()%20;
                    coordinates.y = rand()%20;
                    TmpShape.vertexes.push_back(coordinates);
                }
                TmpShape.vertex_num = 5;
                break;
        }
        Shapes.push_back(TmpShape);
        TmpShape.vertexes.clear();
    };
    void DeletePentagons(){
        for(int i = 0; i < Shapes.size(); i++){
            if(Shapes.at(i).vertex_num == 5){
                Shapes.erase(Shapes.begin() + i);
            }
        }
    };
    void CountAllAngles(){
        int angles = 0;
        for (auto i : Shapes){
            angles += i.vertex_num;
        }
        cout << "Всего вершин в векторе: "<< angles << endl;
    };
    void CountFigures() {
        int triangle = 0, rectangle = 0, pentagon = 0;
        for (auto i : Shapes) {
            switch (i.vertex_num) {
                case 3:
                    triangle++;
                    break;
                case 4:
                    rectangle++;
                    break;
                case 5:
                    pentagon++;
                    break;
            }

        }
        cout << "Треугольников в векторе: " << triangle << endl;
        cout << "Прямоугольников в векторе: " << rectangle << endl;
        cout << "Пятиугольников в векторе: " << pentagon << endl;
    };
    void Sort(){
        int l = 0;
        int r = Shapes.size();
        int i = l;
        while (i < r) {
            if (i == l || Shapes.at(i - 1).vertex_num <= Shapes.at(i).vertex_num) {
                i++;
            } else {
                swap(Shapes.at(i - 1), Shapes.at(i)), i--;
            }
        }
    };
    void PrintVector(){
        for(auto i : Shapes){
            switch (i.vertex_num) {
                case 3:
                    cout << "Треугольник, количество углов: "<< i.vertex_num;
                    break;
                case 4:
                    cout << "Прямоугольник, количество углов: " << i.vertex_num;
                    break;
                case 5:
                    cout << "Пятиугольник, количество углов: " << i.vertex_num;
                    break;
            }
            cout << endl << "Координаты вершин: " <<endl;
            for(auto j : i.vertexes){
                cout <<"x = "<< j.x << "; y = " << j.y << endl;
            }
        }
    };
};



int main() {

    Task shape;
    int i;
    cout << "Сколько фигур добавить? ";
    cin >> i;
    cout << endl;
    for (int j = 0; j < i; j++){
        shape.FillVector();
    }
    shape.PrintVector();
    shape.CountAllAngles();
    shape.CountFigures();
    shape.DeletePentagons();
    shape.PrintVector();
    shape.Sort();
    shape.PrintVector();

    return 0;
}
