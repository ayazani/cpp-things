#include <iostream>
using namespace std;
//найти произведение элементов заданной строки
int funcArr(int* array, int dim1, int dim2, int line_number)
{
    int i, cnt=1;
    for (i = 0; i < dim2; i++)
    {
        cnt *= array[line_number * dim2 + i];
    }
    return (cnt);
}

int funcArr2(int* array, int dim1, int dim2, int line_number)
{1
    int i, cnt = 1;
    for (i = 0; i < dim2; i++)
    {
        cnt *= *(array + line_number * dim2 + i);
    }
    return (cnt);
}

int main() {
    setlocale(LC_ALL, "");
    int i,j, line_number;
    const int size = 3;
    int arr[size][size] = { 1,2,3,4,5,6,7,8,9 };
    for (i = 0; i < size; i++)
    {
        for (j = 0; j < size; j++)
        {
            cout<<arr[i][j]<<" ";
        }
        cout<<endl;
    }
    cout<<"Введите номер строки"<<endl;
    cin>>line_number;
   // line_number--;
    cout<<"Результат первой функции по строке "<<line_number<<endl;
    line_number--;
    cout<<funcArr(*arr, size, size, line_number)<<endl;
    line_number++;
    cout<<"Результат второй функции по строке "<<line_number<<endl;
    line_number--;
    cout<<funcArr2(*arr, size, size, line_number)<<endl;
    return 0;

}
